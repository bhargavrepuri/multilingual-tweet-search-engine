import json, io, pytz
from pytz import timezone
from datetime import datetime
import os

final_json_array = []

def format(file):
	global final_json_array
	tweet = json.load(file)
	ftweet = {}
		# set id
	ftweet['id'] = tweet['id_str']
	# created date
	fmt = '%Y-%m-%dT%H:%M:%SZ'
	temp = datetime.strptime(tweet['created_at'],'%a %b %d %H:%M:%S +0000 %Y').replace(tzinfo=pytz.UTC)
	ftweet['created_at'] = temp.strftime(fmt)
	# set text fields based on language language
	ftweet['lang'] = tweet['lang']
	if tweet['lang'] == 'en':
		ftweet['text_en'] = tweet['text']
	   # ftweet['text_ru'] = ""
	   # ftweet['text_de'] = ""
	elif tweet['lang'] == 'ru':
		#ftweet['text_en'] = ""
		ftweet['text_ru'] = tweet['text']
		#ftweet['text_de'] = ""
	elif tweet['lang'] == 'de':
		#ftweet['text_en'] = ""
		#ftweet['text_ru'] = ""
		ftweet['text_de'] = tweet['text']
	# screen name
	ftweet['screen_name'] = tweet['user']['screen_name']
	# user name
	ftweet['user_name'] = tweet['user']['name']
	# location
	ftweet['location'] = tweet['user']['location']
	# tweet hashtag
	ftweet['tweet_hashtags'] = []
	for tag in tweet['entities']['hashtags']:
		ftweet['tweet_hashtags'].append(tag['text'])
	# tweet extended url
	ftweet['tweet_urls'] = []
	for url in tweet['entities']['urls']:
		ftweet['tweet_urls'].append(url['expanded_url'])

	final_json_array.append(ftweet)

if __name__ == '__main__':
	global final_json_array

#load each file and format it, keep a count, if count reaches 10, dump
	#file = open('tweet_data_en_1.txt', 'r')
	#load(file)

	dir_path = os.path.join(os.getcwd(), 'German')
	count = 0
	for filename in os.listdir(dir_path):
		file = (os.path.join(dir_path, filename))
		if(file.endswith(".txt")):

			print count

			f = open(file,'r')
			format(f)
			count += 1
			if(count == 50):
				break


	with io.open('sample_formatted_de.txt', 'w', encoding='utf-8') as f:
		f.write(unicode(json.dumps(final_json_array, ensure_ascii=False)))
